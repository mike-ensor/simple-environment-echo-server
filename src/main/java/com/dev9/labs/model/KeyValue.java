package com.dev9.labs.model;

public class KeyValue {

    private String key;
    private String value;

    public KeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String keyValuePair() {
        return key + " = " + value;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
}
