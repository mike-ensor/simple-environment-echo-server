package com.dev9.labs.model;

import com.google.common.collect.Lists;

import java.util.List;

public class HomeModel {

    private List<KeyValue> envs;
    private String ipAddress;
    private String fileContents;

    public HomeModel() {
        envs = Lists.newArrayList();
        ipAddress = "not-yet-defined";
        fileContents = "";
    }

    public void setEnvironmentVariables(List<KeyValue> values) {
        envs = values;
    }

    public List<KeyValue> getEnvs() {
        return envs;
    }

    public void addIPAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getFileContents() {
        return fileContents;
    }

    public void setFileContents(String fileContents) {
        this.fileContents = fileContents;
    }
}
