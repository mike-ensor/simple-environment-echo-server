package com.dev9.labs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleEnvironmentEchoServer {

    public static void main(String[] args) {
        SpringApplication.run(SimpleEnvironmentEchoServer.class, args);
    }

}
