package com.dev9.labs.web.controller;

import com.dev9.labs.model.HomeModel;
import com.dev9.labs.model.KeyValue;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    public HomeController() {

    }

    @GetMapping
    public String displayHomePage(@ModelAttribute("model") HomeModel model) {

        model.setEnvironmentVariables(getEnvironmentVaraibles());

        model.addIPAddress(getIPAddress());

        model.setFileContents(getFileContents("/application.properties"));

        return "home/home";
    }

    private String getFileContents(String file) {

        StringBuffer output = new StringBuffer();
        FileSystemResource fileSystemResource = new FileSystemResource(file);

        if (fileSystemResource.exists()) {

            try {
                InputStream inputStream = fileSystemResource.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));

                String line;
                while ((line = br.readLine()) != null) {
                    output.append(line);
                }
                br.close();

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
        return output.toString();
    }

    private List<KeyValue> getEnvironmentVaraibles() {

        List<KeyValue> model = Lists.newArrayList();
        Map<String, String> env = System.getenv();
        for (String envName : env.keySet()) {
            model.add(new KeyValue(envName, env.get(envName)));
        }

        return model;
    }

    private String getIPAddress() {
        InetAddress ip;
        String hostIP = "ip-address-not-found";
        try {
            ip = InetAddress.getLocalHost();
            hostIP = ip.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return hostIP;
    }
}
